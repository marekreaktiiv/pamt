<?php
session_start();

$path = $_SERVER['DOCUMENT_ROOT'] . '/_m';

include_once 'wp-load.php';
include_once 'wp-includes/wp-db.php';
include_once 'wp-includes/pluggable.php';

$m2_price = $_SESSION["m2_price"];
$areawidth = $_POST["areawidth"];
$areadepth = $_POST["areadepth"];
$tableAreaPrice = $_POST["tableArea"] * $_SESSION["muugilaud_m2_hind"];
$tableAreaRoofPrice = $_POST["tableAreaWithRoof"] * $_SESSION["katusega_muugilaud_m2_hind"];
$organizerTentPrice = $_POST["placeInOrganizerTent"] * $_SESSION["muugikoht_korraldaja_kasitootelgis_m2_hind"];

echo 'name: ' . $_POST["name"] . '</br>';
echo 'registercode: ' . $_POST["registercode"] . '</br>';
echo 'email: ' . $_POST["email"] . '</br>';
echo 'address: ' . $_POST["address"] . '</br>';
echo 'productType: ' . $_POST["productType"] . '</br>';
echo 'productlist: ' . $_POST["productlist"] . '</br>';
echo 'extrainfo: ' . $_POST["extrainfo"] . '</br>';
echo 'areawidth: ' . $_POST["areawidth"] . '</br>';
echo 'areadepth: ' . $_POST["areadepth"] . '</br>';
echo 'payment: ' . $_POST["payment"] . '</br>';
echo 'electricity: ' . $_POST["electricity"] . '</br>';
echo 'tableArea: ' . $_POST["tableArea"] . '</br>';
echo 'tableAreaWithRoof: ' . $_POST["tableAreaWithRoof"] . '</br>';
echo 'placeInOrganizerTent: ' . $_POST["placeInOrganizerTent"] . '</br>';
echo 'vehicle: ' . $_POST["vehicle"] . '</br>';
echo 'pricetable: ' . $_POST["pricetable"] . '</br>';
echo 'rules: ' . $_POST["rules"] . '</br>';

echo '</br>';
echo '</br>';

echo 'noElectricity: ' . $_SESSION["noElectricity"] . '</br>';
echo 'v220_price: ' . $_SESSION["v220_price"] . '</br>';
echo 'v400_price: ' . $_SESSION["v400_price"] . '</br>';

echo 'noVehicle: ' . $_SESSION["noVehicle"] . '</br>';
echo' vehiclecar: ' . $_SESSION["vehiclecar"] . '</br>';
echo 'vehiclebus: ' . $_SESSION["vehiclebus"] . '</br>';

echo 'muugilaud_m2_hind: ' . $_SESSION["muugilaud_m2_hind"] . '</br>';
echo 'katusega_muugilaud_m2_hind: ' . $_SESSION["katusega_muugilaud_m2_hind"] . '</br>';
echo 'muugikoht_korraldaja_kasitootelgis_m2_hind: ' . $_SESSION["muugikoht_korraldaja_kasitootelgis_m2_hind"] . '</br>';

echo '</br>';
echo '</br>';

$errors = 0;

if($_POST["payment"] == 'sularahas') :
    $payment = $m2_price['kohapeal_hinnad'];
elseif($_POST["payment"] == 'pangalingiga'):
    $payment = $m2_price['ettemaks_hinnad'];
else:
    $errors++;
endif;

if($_POST["productType"] == 'tööstuskaup') :
    $sizePrice = $payment['toostuskaup'];
elseif($_POST["productType"] == 'käsitöö'):
    $sizePrice = $payment['kasitoo'];
elseif($_POST["productType"] == 'toidukaup'):
    $sizePrice = $payment['toidukaup'];
elseif($_POST["productType"] == 'toitlustaja'):
    $sizePrice = $payment['toitlustaja'];
else:
    $errors++;
endif;

if($_POST["vehicle"] == 'no') :
    $vehicle = $_SESSION["noVehicle"];
elseif($_POST["vehicle"] == 'car'):
    $vehicle = $_SESSION["vehiclecar"];
elseif($_POST["vehicle"] == 'bus'):
    $vehicle = $_SESSION["vehiclebus"];
else:
    $errors++;
endif;

if($_POST["electricity"] == 'noelectricity') :
    $electricity = $_SESSION["noElectricity"];
elseif($_POST["electricity"] == '220V'):
    $electricity = $_SESSION["v220_price"];
elseif($_POST["electricity"] == '400V'):
    $electricity = $_SESSION["v400_price"];
else:
    $errors++;
endif;

$totalPrice = $sizePrice * ($areawidth * $areadepth) + $electricity + $tableAreaPrice + $tableAreaRoofPrice + $organizerTentPrice + $vehicle;
echo 'total price: ' . $totalPrice;

global $wpdb, $name, $email;
date_default_timezone_set("Europe/Tallinn");
$date = date("Y-m-d H:i:s");
$table_name = "sale_form_data";
$wpdb->insert(
    $table_name, array(
        'time' => $date,
        'name' => $_POST["name"],
        'registercode' => $_POST["registercode"],
        'email' => $_POST["email"],
        'address' => $_POST["address"],
        'productType' => $_POST["productType"],
        'productlist' => $_POST["productlist"],
        'extrainfo' => $_POST["extrainfo"],
        'areawidth' => $_POST["areawidth"],
        'areadepth' => $_POST["areadepth"],
        'payment' => $_POST["payment"],
        'electricity' => $_POST["electricity"],
        'tableArea' => $_POST["tableArea"],
        'tableAreaWithRoof' => $_POST["tableAreaWithRoof"],
        'placeInOrganizerTent' => $_POST["placeInOrganizerTent"],
        'vehicle' => $_POST["vehicle"],
        'totalPrice' => $totalPrice
    )
);

$to = 'marek@reaktiiv.ee';
$subject = 'PAMT laada registreerimisvorm';
$headers[] = 'From: PAMT <noreply@pampt.ee>';
$headers[] = 'Content-Type: text/html; charset=UTF-8';
$body = '
<span>päringu sooritamise kellaaeg: '. $date . '</span></br>
<span>Firma või eraisiku nimi: '. $_POST["name"] . '</span></br>
<span>Registrikood või isikukood: '. $_POST["registercode"] . '</span></br>
<span>E-posti aadress: '. $_POST["email"] . '</span></br>
<span>Aadress: '. $_POST["address"] . '</span></br>
<span>Kauba liik: '. $_POST["productType"] . '</span></br>
<span>Kaupade teenuste loetelu: '. $_POST["productlist"] . '</span></br>
<span>Lisateave: '. $_POST["extrainfo"] . '</span></br>
<span>Müügipinna LAIUS: '. $_POST["areawidth"] . 'm' . '</span></br>
<span>Müügipinna SÜGAVUS: '. $_POST["areadepth"] . 'm' . '</span></br>

<span>Makseviis: '. $_POST["payment"] . '</span></br>

<span>Elekter: '. $_POST["electricity"] . '</span></br>

<span>Lauapinda kokku: '. $_POST["tableArea"] . 'm' . '</span></br>
<span>Katusega müügilaud: '. $_POST["tableAreaWithRoof"] . 'TK' . '</span></br>
<span>Müügikoht korraldaja käsitöötelgis: '. $_POST["placeInOrganizerTent"] . 'm<sup>2</sup>' . '</span></br>

<span>Mootorsõiduki paigaldamine müügiplatsile: '. $_POST["vehicle"] . '</span></br>

<span>Hind kokku: '. $totalPrice . '€' . '</span></br>
';


wp_mail($to, $subject, $body, $headers);

?>