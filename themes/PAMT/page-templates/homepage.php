<?php
/*
Template Name: Homepage
Description: Homepage
*/
?>
<?php get_header(); ?>

    <section>
      <div class="wrapper">
         <div class="column cs-75">
            <div class="frontpage_events">
                <?php echo frontpageView(); ?>
            </div>
            <a class="all_events_btn" href="<?php echo get_site_url(); ?>/koik-sundmused">Kõik Sündmused</a>
         </div>
         <?php get_sidebar(); ?>
      </div>
   </section>

<?php get_footer(); ?>