<?php
/*
Template Name: Calendar
Description: Calendar
*/
?>
<?php get_header(); ?>

   <section>
      <div class="wrapper">
         <div class="column cs-80">
            <p>Olete valinud kõik Paides toimuvad <span><?php echo selectedDate(); ?></span> sündmused</p>
            <div class="selected_events">
                  <?php echo selectedEvents(); ?>
            </div>
         </div>
         <?php get_sidebar(); ?>
      </div>
   </section>

<?php get_footer(); ?>