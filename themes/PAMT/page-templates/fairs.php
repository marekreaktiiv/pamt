<?php
/*
Template Name: Fairs
Description: Fairs
*/
?>
<?php get_header(); ?>
   <section>
        <div class="wrapper">
            <div class="column cs-75">
                <?php
                $args = array(
                    'post_type' => 'events',
                    'post_status' => 'publish',
                    'category_name' => 'laadad'
                );

                $arr_posts = new WP_Query( $args );

                if ($arr_posts->have_posts()) : ?>

                    <div class="fairsContainer">
                        <h3>Laatade ajakava</h3>
                        <div class="fairsInfo">
                            <p>Laadakorraldaja e-post</br><a href="mailto:laadad@pamt.ee">laadad@pamt.ee</a></p>
                            <p>Laadakorraldaja tel</br><strong>55620052</strong></p>
                            <p><a href="http://labor.reaktiiv.ee/_m/pamt/laadad/hinnakiri/">Hinnakirjad</a></br><a href="http://labor.reaktiiv.ee/_m/pamt/laadad/kauplemise-reeglid/">Kauplemise reeglid</a></p>
                        </div>
                        <div class="futureFairs">
                            <?php
                            $category = "laadad";
                            $fairEvents = getEventsCategoryPosts($category);
                            $fairEventsSorted = PostOrderingByDate($fairEvents);

                            while ($arr_posts->have_posts()) :
                                $arr_posts->the_post();
                                $range = controllRange(get_the_ID());
                                $filter = filterEByDate($range, get_the_ID()); ?>
                                <?php  if($filter == '1'): ?>
                                    <div class="fair">
                                        <div class="leftSide">
                                            <h3 class="dateTitle"><?php echo get_the_title(); ?></h3>
                                            <?php if(get_field('dates')): ?>
                                                <?php while(has_sub_field('dates')): ?>

                                                    <h3><?php echo getEDate(get_sub_field('date')); ?></h3>

                                                <?php endwhile; ?>
                                            <?php endif; ?>

                                            <p class="location"><?php the_field('hall'); ?></p>

                                            <?php if(get_field('dates')):
                                                $fieldDates = get_field('dates');
                                                $counter = 0; ?>
                                                <p class="time">
                                                <?php foreach($fieldDates as $fieldDate):
                                                    if($fieldDate['times']):
                                                        $times = $fieldDate['times'];

                                                        foreach($times as $time): ?>
                                                            <?php echo $time['time']; ?>
                                                            <?php if($counter == 0):
                                                                echo ' - ';
                                                            endif; ?>

                                                            <?php $counter++; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif;
                                                endforeach; ?>
                                                </p>
                                            <?php endif; ?>
                                        </div>
                                        <?php if(get_field('reg_form_link')['0'] !== 'yes'): ?>
                                            <div class="rightSide">
                                                <a href="<?php echo get_home_url(); ?>/laadad/registreeru-kauplema?fair=<?php echo get_the_ID(); ?>">Registreeru</br>Kauplema</a>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            <?php endwhile; ?>

                            <div class="nb">
                                <p>NB! Toitlustajatel palume enne registreerumist laadakorraldajaga ühendust võtta, et selgitada kas toitlustuskohti ja -vajadust laadal on.</p>
                            </div>
                        </div>
                        <div class="pastFairs">
                            <h3>Toimunud laadad</h3>
                            <?php while ($arr_posts->have_posts()) :
                                $arr_posts->the_post();
                                $range = controllRange(get_the_ID());
                                $filter = filterEByDate($range, get_the_ID());?>
                                <?php  if($filter == '0'): ?>
                                    <div class="fair">
                                        <div class="leftSide">
                                            <h3 class="dateTitle"><?php echo get_the_title(); ?></h3>
                                            <?php if(get_field('dates')): ?>
                                                <?php while(has_sub_field('dates')): ?>

                                                    <h3><?php echo getEDate(get_sub_field('date')); ?></h3>

                                                <?php endwhile; ?>
                                            <?php endif; ?>

                                            <p class="location"><?php the_field('hall'); ?></p>

                                            <?php if(get_field('dates')):
                                                $fieldDates = get_field('dates');
                                                $counter = 0; ?>
                                                <p class="time">
                                                <?php foreach($fieldDates as $fieldDate):
                                                    if($fieldDate['times']):
                                                        $times = $fieldDate['times'];

                                                        foreach($times as $time): ?>
                                                            <?php echo $time['time']; ?>
                                                            <?php if($counter == 0):
                                                                echo ' - ';
                                                            endif; ?>

                                                            <?php $counter++; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif;
                                                endforeach; ?>
                                                </p>
                                            <?php endif; ?>
                                        </div>
                                        <div class="rightSide">
                                            <a href="<?php echo get_field('gallery_link'); ?>">Vaata</br>galeriid</a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endwhile; ?>
                            <!-- <?php
                            $category = "laadad";
                            $fairEvents = getEventsCategoryPosts($category);
                            $fairEventsSorted = PostOrderingByDate($fairEvents);

                            foreach($fairEventsSorted as $event):
                                $range = controllRange(get_the_ID());
                                $filter = filterEByDate($range, get_the_ID());
                                $rangePast = controllRange(get_the_ID());
                                $filterPast = filterEByDate($rangePast, get_the_ID());
                                ?>
                               <?php  if($filterPast == '0'): ?>
                                    <div class="fair">
                                        <div class="leftSide">
                                            <h3 class="dateTitle"><?php echo getEDate(get_field('date', $event->ID)) . ' - ' . get_the_title($event->ID); ?></h3>
                                            <p class="location"><?php the_field('hall'); ?></p>
                                            <p class="time"><?php echo get_field('time', $event->ID) . ' - ' . get_field('time_end', $event->ID); ?></p>
                                        </div>
                                        <div class="rightSide">
                                            <a href="<?php echo get_home_url(); ?>/laadad/registreeru-kauplema/">Registreeru</br>Kauplema</a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?> -->
                        </div>
                    </div>
                <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
            <?php get_sidebar(); ?>
        </div>
   </section>
<?php get_footer(); ?>