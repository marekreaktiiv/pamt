<?php
session_start();
/*
Template Name: Register form
Description: Register form
*/

$url =  "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
$escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
$formId = end(explode('=', $escaped_url));
?>

<?php get_header(); ?>
   <section>
        <div class='wrapper'>
            <div class="column cs-75">
                <?php
                $args = array(
                    'post_type' => 'events',
                    'post_status' => 'publish',
                    'category_name' => 'laadad'
                );

                $arr_posts = new WP_Query( $args );

                if ($arr_posts->have_posts()) : ?>

                    <div class='fairsContainer'>
                        <?php while ($arr_posts->have_posts()) :
                            $arr_posts->the_post(); ?>
                            <?php if(get_post()->ID == $formId):
                                setlocale(LC_TIME, 'et_EE');
                                $getFairDate = strtotime(get_field('date'));
                                $weekday = utf8_encode(strftime('%A', $getFairDate)); ?>
                                <h3 class='dateTitle'><?php echo get_the_title() . ' - ' . '<span>' . $weekday . '</span>' . ' ' . getEDate(get_field('date')); ?></h3>
                                <h3 class="location"><?php the_field('hall'); ?></h3>
                                <h3 class="time"><?php echo get_field('time') . ' - ' . get_field('time_end'); ?></h3>

                                <div class='fairsInfo'>
                                    <p>Laadakorraldaja e-post</br><a href='mailto:laadad@pamt.ee'>laadad@pamt.ee</a></p>
                                    <p>Laadakorraldaja tel</br><strong>55620052</strong></p>
                                    <p><a href='http://labor.reaktiiv.ee/_m/pamt/laadad/hinnakiri/'>Hinnakirjad</a></br><a href='http://labor.reaktiiv.ee/_m/pamt/laadad/kauplemise-reeglid/'>Kauplemise reeglid</a></p>
                                </div>
                                <div class="fairContent">
                                    <?php the_content(); ?>
                                </div>
                                <form action="http://labor.reaktiiv.ee/_m/pamt/send-form.php" method="post" ng-app="myApp" ng-controller="myCtrl">
                                    <div class="formFields">
                                        <p><strong>Registreerun kauplema</strong></p>

                                        <label for="name" required>Firma või eraisiku nimi</label>
                                        <span>(sisestamise järjekord: firma nimi ja õigusliku vormi tähis (AS, OÜ…)või perekonnanimi ja eesnimi.)</span>
                                        <input name="name" type="text" required>

                                        <label for="registercode" required>Registrikood või isikukood</label>
                                        <input name="registercode" type="text" required>

                                        <label for="email">E-posti aadress</label>
                                        <input name="email" type="text">

                                        <label for="address" required>Aadress</label>
                                        <input name="address" type="text" required>

                                        <label class="container">Tööstuskaup
                                            <input type="radio" checked="checked" name="productType" value="tööstuskaup">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="container">Käsitöö
                                            <input type="radio" name="productType" value="käsitöö">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="container">Toidukaup
                                            <input type="radio" name="productType" value="toidukaup">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="container">Toitlustaja
                                            <input type="radio" name="productType" value="toitlustaja">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label for="productlist" required>Kaupade teenuste loetelu</label>
                                        <textarea name="productlist" rows="10" cols="30" required></textarea>

                                        <label for="extrainfo">Lisateave</label>
                                        <textarea name="extrainfo" rows="10" cols="30"></textarea>

                                        <div class="saleAreaSize">
                                            <div class="areawidth">
                                                <label for="areawidth" required>Müügipinna LAIUS (m)</label>
                                                <span>m</span>
                                                <input name="areawidth" type="number" required>
                                            </div>
                                            <div class="areadepth">
                                                <label for="areadepth" required>Müügipinna SÜGAVUS (m)</label>
                                                <span>m</span>
                                                <input name="areadepth" type="number" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="formFields">
                                        <p><strong class="required">Makseviis</strong></p>
                                        <label class="container">SULARAHA (Maksmine laadahommikul kohapeal)
                                            <input type="radio" name="payment" value="sularahas">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="container">Pangalink
                                            <input type="radio" checked="checked" name="payment" value="pangalingiga">
                                            <span class="checkmark"></span>
                                        </label>
                                        <div class="paymentInfo">
                                            <span>m2 hind tasumisel pangalingiga <strong>3.00€</strong></span>
                                            <span>m2 hind tasumisel laadahommikul sularahas <strong>5.00€</strong></span>
                                        </div>
                                    </div>
                                    <div class="formFields">
                                        <p><strong>Täiendava lisatasu eest:</strong></p>
                                        <p>Elekter</p>
                                        <label class="container">ei soovi
                                            <input type="radio" checked="checked" name="electricity" value="noelectricity">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="container">220V; 16A; kuni 3 kW <strong>10.00€</strong>
                                            <input type="radio" name="electricity" value="220V">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="container">400V; 16A; kuni 4 kW <strong>25.00€</strong>
                                            <input type="radio" name="electricity" value="400V">
                                            <span class="checkmark"></span>
                                        </label>

                                        <p>Müügilaud / Müügikoht</p>
                                        <div class="saleAreaSize salePlaces">
                                            <div class="singlePlace">
                                                <span class="before">Lauapinda kokku</span>
                                                <input name="tableArea" type="number" price="2">
                                                <span>m</span>
                                            </div>

                                            <div class="singlePlace">
                                                <span class="before">Käsitöötelki vaid - 1m hind <strong>2.00€</strong></span>
                                            </div>

                                            <div class="singlePlace">
                                                <span class="before">Katusega müügilaud</span>
                                                <input name="tableAreaWithRoof" type="number">
                                                <span>TK</span>
                                            </div>

                                            <div class="singlePlace">
                                                <span class="before">Müügikoht korraldaja käsitöötelgis</span>
                                                <input name="placeInOrganizerTent" type="number">
                                                <span>m2  (m2 hind <strong>2.00€</strong>)</span>
                                            </div>
                                        </div>

                                        <p>Mootorsõiduki paigaldamine müügiplatsile</p>
                                        <label class="container">ei soovi
                                            <input type="radio" checked="checked" name="vehicle" value="no">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="container">Sõiduauto (võimalik alates 2x2m müügipinnaga) <strong>8.00€</strong>
                                            <input type="radio" name="vehicle" value="car">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="container">Buss / kaubik (võimalik alates 3x3m müügipinnaga) <strong>15.00€</strong>
                                            <input type="radio" name="vehicle" value="bus">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <?php
                                        $m2_price = get_field('m2_hind');
                                        $_SESSION["m2_price"] = $m2_price;

                                        $vehicle = get_field('mootorsoiduk');
                                        $_SESSION["noVehicle"] = '0';
                                        $_SESSION["vehiclecar"] = $vehicle['soiduauto'];
                                        $_SESSION["vehiclebus"] = $vehicle['buss_ja_kaubik'];

                                        $elekter = get_field('elekter');
                                        $_SESSION["noElectricity"] = '0';
                                        $_SESSION["v220_price"] = $elekter['v220_price'];
                                        $_SESSION["v400_price"] = $elekter['v400_price'];

                                        $muugilaud = get_field('muugilaud');
                                        $muugilaud_m2_naita = $muugilaud['muugilaud_m2_naita'];
                                        $_SESSION["muugilaud_m2_hind"] = $muugilaud['muugilaud_m2_hind'];

                                        $katusega_muugilaud = get_field('katusega_muugilaud');
                                        $katusega_muugilaud_m2_naita = $katusega_muugilaud['katusega_muugilaud_m2_naita'];
                                        $_SESSION["katusega_muugilaud_m2_hind"] = $katusega_muugilaud['katusega_muugilaud_m2_hind'];

                                        $muugikoht_korraldaja_kasitootelgis = get_field('muugikoht_korraldaja_kasitootelgis');
                                        $muugikoht_korraldaja_kasitootelgis_naita = $muugikoht_korraldaja_kasitootelgis['muugikoht_korraldaja_kasitootelgis_naita'];
                                        $_SESSION["muugikoht_korraldaja_kasitootelgis_m2_hind"] = $muugikoht_korraldaja_kasitootelgis['muugikoht_korraldaja_kasitootelgis_m2_hind'];
                                    ?>
                                    <div class="formFooter">
                                        <p><strong>Hind kokku: <span class="priceTogether">0</span>€</strong></p>
                                        <label class="container">
                                            <input type="checkbox" value="nõustun" name="pricetable" required>
                                            <span class="checkmark"></span>
                                            <span class="required">Olen tutvunud hinnakirjaga</span> <a href="">Hinnakiri</a>
                                        </label>
                                        <label class="container">
                                            <input type="checkbox" value="nõustun" name="rules" required>
                                            <span class="checkmark"></span>
                                            <span class="required">Olen tutvunud ja kohustun järgima kauplemise reeglitega</span> <a href="">Kauplemise reeglid</a>
                                        </label>
                                        <input type="submit">
                                    </div>
                                </form>

                                <script>
                                    var m2_price, sizePrice, areawidth, areadepth, checkPayment, payment, productType, electricity, electricityPrice, tableAreaPrice, tableAreaRoofPrice, organizerTentPrice, vehicle, totalPrice;

                                    function updatePrice () {
                                        m2_price = <?php echo json_encode($m2_price); ?>;
                                        checkPayment = jQuery('input[name=payment]:checked').val();
                                        productType = jQuery('input[name=productType]:checked').val();
                                        areawidth = jQuery('input[name="areawidth"]').val();
                                        areadepth = jQuery('input[name="areadepth"]').val();
                                        electricity = jQuery('input[name=electricity]:checked').val();

                                        tableAreaPrice = jQuery('input[name="tableArea"]').val() * <?php echo $_SESSION["muugilaud_m2_hind"]; ?>;
                                        tableAreaRoofPrice = jQuery('input[name="tableAreaWithRoof"]').val() * <?php echo $_SESSION["katusega_muugilaud_m2_hind"]; ?>;
                                        organizerTentPrice = jQuery('input[name="placeInOrganizerTent"]').val() * <?php echo $_SESSION["muugikoht_korraldaja_kasitootelgis_m2_hind"]; ?>;

                                        vehicle = jQuery('input[name=vehicle]:checked').val();

                                        if(checkPayment == 'sularahas') {
                                            payment = 'kohapeal_hinnad';
                                        } else if(checkPayment == 'pangalingiga') {
                                            payment = 'ettemaks_hinnad';
                                        }

                                        if(productType == 'tööstuskaup') {
                                            sizePrice = m2_price[payment]['toostuskaup'];
                                        } else if(productType == 'käsitöö') {
                                            sizePrice = m2_price[payment]['kasitoo'];
                                        } else if(productType == 'toidukaup') {
                                            sizePrice = m2_price[payment]['toidukaup'];
                                        } else if(productType == 'toitlustaja') {
                                            sizePrice = m2_price[payment]['toitlustaja'];
                                        }

                                        if(electricity == 'noelectricity') {
                                            electricityPrice = <?php echo $_SESSION["noElectricity"]; ?>;
                                        } else if(electricity == '220V') {
                                            electricityPrice = <?php echo $_SESSION["v220_price"]; ?>;
                                        } else if(electricity == '400V') {
                                            electricityPrice = <?php echo $_SESSION["v400_price"]; ?>;
                                        } else {
                                            $errors++;
                                        }

                                        if(vehicle == 'no') {
                                            vehiclePrice = <?php echo $_SESSION["noVehicle"]; ?>;
                                        } else if(vehicle == 'car') {
                                            vehiclePrice = <?php echo $_SESSION["vehiclecar"]; ?>;
                                        } else if(vehicle == 'bus') {
                                            vehiclePrice = <?php echo $_SESSION["vehiclebus"]; ?>;
                                        } else {
                                            $errors++;
                                        }

                                        totalPrice = sizePrice * (areawidth * areadepth) + electricityPrice + tableAreaPrice + tableAreaRoofPrice + organizerTentPrice + vehiclePrice;

                                        jQuery('.priceTogether').html(totalPrice);
                                    }

                                    jQuery(document).ready(function() {
                                        updatePrice();

                                        jQuery('input').on('click', function() {
                                            updatePrice();
                                        });

                                        jQuery('input[type=number]').keyup(function() {
                                            updatePrice();
                                        });
                                    });
                                </script>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
            <?php get_sidebar(); ?>
        </div>
   </section>
<?php get_footer(); ?>