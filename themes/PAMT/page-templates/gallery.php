<?php
/*
Template Name: Gallery
Description: Gallery
*/
?>
<?php get_header(); ?>

   <section>
      <div class="wrapper">
         <div class="column cs-75">
               <?php
         $args = array(
    'post_type' => 'gallery',
    'post_status' => 'publish'

);

$arr_posts = new WP_Query( $args );

 

if ($arr_posts->have_posts()) :
    $counter = 0; ?>

    <div class="gallery-posts-container">

    <?php while ($arr_posts->have_posts()) :

        $galleryCounter = 0;

        $arr_posts->the_post();

        ?>

        <div id="gallery-post-<?php the_ID(); ?>" class="gallery-post">

            <a class="dev_gallery" data-gallery = "<?php echo $counter; ?>">
                <img src="<?php echo get_the_post_thumbnail_url(); ?>" />
            </a>

            <?php $galleryGenerator = get_field('gallery_generator'); ?>

            <div id="gallery-<?php echo $counter; ?>" class="hidden">
            
                
                
                <?php foreach ($galleryGenerator as $image) : ?>          

                    <div class="single_gallery_image" id="gallery-<?php echo $counter; ?>_<?php echo $galleryCounter; ?>" data-src="<?php echo $image['url']; ?>">

                        <img src="<?php echo $image['url']; ?>" />

                    </div> 
        
                <?php endforeach; ?>

            </div>

        <p><?php the_field('event_date'); ?></p>

        <?php the_content(); ?>

        </div>

        <?php

        $counter++;

    endwhile; ?>

    </div>

<?php endif; ?>


         </div>
         <?php get_sidebar(); ?>
      </div>
   </section>

<?php get_footer(); ?>