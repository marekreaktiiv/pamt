<?php
/*
Template Name: Topic
Description: Topic
*/
?>
<?php get_header(); ?>

   <section>
      <div class="wrapper">
         <div class="column cs-80">
            <div class="selected_events">
                  <?php echo selectedTopicEvents($post->post_title); ?>
            </div>
         </div>
         <?php get_sidebar(); ?>
      </div>
   </section>

<?php get_footer(); ?>