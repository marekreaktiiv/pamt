<?php
/*
Template Name: All Events
Description: All Events
*/
?>
<?php get_header(); ?>

   <section>
      <div class="wrapper">
         <div class="column cs-75">
            <h3>KÕIK SÜNDMUSED</h3>
            <div class="selected_events">

                  <!-- <div class="all_events">
                        <?php while ( have_posts() ) : the_post(); ?>
                              <?php the_content(); ?>
                        <?php endwhile; ?>
                        <?php wp_reset_query(); ?>
                  </div> -->


                  <?php echo allEvents($post->post_title); ?>
            </div>
         </div>
         <?php get_sidebar(); ?>
      </div>
   </section>

<?php get_footer(); ?>