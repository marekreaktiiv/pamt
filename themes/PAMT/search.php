<?php get_header(); ?>

<section>
	<div class="wrapper">
		<div class="column cs-75">
			<div class="searchContainer">
				<h3>OTSINGU VASTED</h3>
		
				<?php if (have_posts()) : ?>

					<?php while (have_posts()) : the_post(); ?>

						<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

							<p><?php the_title(); ?> <a style="color: #9cd0d6;" href="<?php echo get_the_permalink(); ?>">VAATA</a></p>

						</div>

					<?php endwhile; ?>

				<?php else : ?>

					<h2>Ühtegi vastet ei leitud.</h2>

				<?php endif; ?>
			</div>
		</div>
		<?php get_sidebar(); ?>
	</div>
</section>

<?php get_footer(); ?>
