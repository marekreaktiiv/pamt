<!DOCTYPE html>

<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=yes"/>
   <meta name="HandheldFriendly" content="true" />
   <meta name="apple-mobile-web-app-capable" content="YES" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

   <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

   <script src="<?php echo get_template_directory_uri(); ?>/js/effects.js?ver=1" type="text/javascript"></script>

   <script src="<?php echo get_template_directory_uri(); ?>/js/lib/lightgallery-all.min.js?ver=1" type="text/javascript"></script>
   <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/lib/lightgallery.min.css" type="text/css" />

</head>

<body <?php body_class(); ?>>

	<div id="header">

		<div class="wrapper">

			<a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" /></a>

			<?php nav('header-menu', 'Main Menu EST', 'menu-main-menu-est-container', '<span class="borderefect"></span>'); ?>

			<div class="links">
				<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png" /></a>
				<a href="#" class="link_to_paide_theater">PAIDE TEATER</a>
			</div>

			<div class="mobile-menu-btn">
				<span></span>
				<span></span>
				<span></span>
			</div>

		</div>

	</div>

	<?php if(is_front_page()): ?>
		<?php get_template_part( 'partials/header', 'slider' ); ?>
	<?php endif; ?>

	<div class="calendar-container">
		<div class="wrapper">
			<?php get_template_part( 'partials/header', 'calendar' ); ?>
		</div>
	</div>