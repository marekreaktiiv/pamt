// Effects.js
jQuery(document).ready(function(){

    jQuery('.mobile-menu-btn').click(function(){
        if(jQuery(this).hasClass('open')) {
            jQuery(this).removeClass('open');
            jQuery('#sidebar-menu').removeClass('open');
        } else {
            jQuery(this).addClass('open');
            jQuery('#sidebar-menu').addClass('open');
        }
    });

    jQuery('.arrow').click(function(){
        if(jQuery(this).parent('li').hasClass('subMenuOpen')) {
            jQuery('.menu-item-has-children').removeClass('subMenuOpen');
        } else {
            jQuery('.menu-item-has-children').removeClass('subMenuOpen');
            jQuery(this).parent('li').addClass('subMenuOpen');
        }
    });
  
    jQuery('#featured-image').lightGallery(); 
    jQuery('#video-gallery').lightGallery(); 

    jQuery('.event-item, .frontpage_event_single').click(function(){
        var link = jQuery(this).attr('data-target');
        window.location.href = link;
        return false;
    }); 

    /* LightGallery for gallery page */
    var galleries = document.querySelectorAll('*[id^="gallery-"]');
    for (var i = galleries.length; i--;){
        var galleryID = String(jQuery(galleries[i]).attr('id'));
        jQuery("#"+galleryID).lightGallery({
            download: false,
            share: false,
            selector: '.single_gallery_image'
        });
    }

    /* Begin LightGallery from button click */
    jQuery('.dev_gallery').on('click touchend', function(e) {
        var openGallery = jQuery(this).attr('data-gallery');
        jQuery("#gallery-"+openGallery+" div:first-child").click();
    });
});