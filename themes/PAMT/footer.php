		<div id="footer">
			<div class="wrapper">
				<div class="footer_menu">
					<?php nav('footer-menu', 'Footer Menu EST', 'menu-footer-menu-est-container', '<span class="arrow"></span>'); ?>
				</div>
				<div class="cooperation_partner">
					<p>Koostööpartnerid</p>
					<a href="https://www.artun.ee/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/eka.png" /></a>
				</div>
			</div>
		</div>

	<?php wp_footer(); ?>

</body>

</html>