<?php get_header(); ?>


<div class="wrapper">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="column cs-80">
			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

				<div class="singleLeftSide">
					<?php if (has_post_thumbnail()) : ?>
						<div id="featured-image">
							<a href="<?php the_post_thumbnail_url(); ?>" data-src="<?php the_post_thumbnail_url(); ?>">
								<img src="<?php the_post_thumbnail_url(); ?>" />
							</a>
						</div>
					<?php endif; ?>
					<!-- <?php if (have_rows('trailer')): ?>
						<div id="video-gallery">
						    <?php while (have_rows('trailer')) : the_row(); ?>
						        <a href="<?php the_sub_field('trailer_link'); ?>" data-poster="<?php the_sub_field('trailer_thumbnail'); ?>">
								    <img src="<?php the_sub_field('trailer_thumbnail'); ?>" />
								</a>
						    <?php endwhile; ?>
						</div>
					<?php endif; ?> -->
				</div>

				<div class="entry">

					<h2><?php the_title(); ?></h2>
					<?php if(get_field('dates', $event->ID)):
						$dates = get_field('dates', $event->ID);
						if(get_field('ajavahemik', $event->ID)['0'] == 'yes'): ?>
							<p><?php echo getEDate($dates['0']['date']); ?> - <?php echo getEDate($dates['1']['date']); ?></p>
							<p><?php echo $dates['0']['times']['0']['time']; ?> - <?php echo $dates['0']['times']['1']['time']; ?></p>
						<?php else:
							while(has_sub_field('dates')): ?>
								<p><?php echo getEDate(get_sub_field('date')); ?></p>
								<p><?php the_sub_field('time'); ?></p>
							<?php endwhile;
						endif;
					endif; ?>
					<p class="event-hall"><?php the_field('hall'); ?></p>

					<p class="event-buy-ticket">
						<?php if(get_field('free')): ?>
								<a>Üritus on tasuta</a>
						<?php else: ?>
							<?php if(get_field('buy_ticket_link')): ?>
								<a href="<?php the_field('buy_ticket_link'); ?>">Osta pilet</a>
							<?php endif; ?>
						<?php endif; ?>
					</p>

					<?php the_content(); ?>

					<p class="goBack"></p><a href="javascript:history.go(-1)">< TAGASI</a></p>

					<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>



					<?php the_tags( 'Tags: ', ', ', ''); ?>



				</div>



				<!-- <?php edit_post_link('Edit this entry','','.'); ?> -->



			</div>



			<!-- <?php comments_template(); ?> -->

		</div>

	<?php endwhile; endif; ?>



<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>