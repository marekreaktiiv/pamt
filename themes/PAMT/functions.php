<?php

	// RSS
	automatic_feed_links();

	// Load jQuery
	if ( !is_admin() ) {
	   wp_deregister_script('jquery');
	   wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"), false);
	   wp_enqueue_script('jquery');
	}

	// Puhastame <head> sektsiooni
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');

	// Moodulite / Widgettite positsioonid
	if (function_exists('register_sidebar')) {
		register_sidebar(array(
			'name' => 'Sidebar Widgets',
			'id'   => 'sidebar-widgets',
			'description'   => 'These are widgets for the sidebar.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2>',
			'after_title'   => '</h2>'
		));
	}

	// Main menu üleval
	function register_bt_menus() {
		register_nav_menus(
			array(
				'main-menu' => __( 'Main Menu' )
			)
		);
	}

	add_action( 'init', 'register_bt_menus' );

	// Search results highlighting
	function wps_highlight_results($text){
		if(is_search()){
			$sr = get_query_var('s');
			$keys = explode(" ",$sr);
			$text = preg_replace('/('.implode('|', $keys) .')/iu', '<strong class="search-excerpt">'.$sr.'</strong>', $text);
		}
		return $text;
	}

	add_filter('the_excerpt', 'wps_highlight_results');

	add_filter('the_title', 'wps_highlight_results');

	// POST THUMBNAILS INIT
	add_theme_support( 'post-thumbnails' );

	// Shorten text
	function stripAndShorten($input,$length = 275) {
		$output = strip_tags($input);
		$output = mb_substr($output, 0, $length);
		return $output;
	}

	// Custom excerpt length
	function custom_excerpt_length( $length ) {
		return 20;
	}

	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

	add_action('init', 'create_post_type_events');

	function create_post_type_events(){
		register_taxonomy_for_object_type('category', 'Events');
		register_taxonomy_for_object_type('post_tag', 'events');
		register_post_type('events',
			array(
				'labels' => array(
					'name' => __('Events', 'events'),
					'singular_name' => __('Event', 'events'),
					'add_new' => __('Add New Event', 'events'),
					'add_new_item' => __('Add New Events', 'events'),
					'edit' => __('Edit', 'events'),
					'edit_item' => __('Edit Event', 'events'),
					'new_item' => __('New Event', 'events'),
					'view' => __('View Events', 'events'),
					'view_item' => __('View Event', 'events'),
					'search_items' => __('Search Events', 'events'),
					'not_found' => __('No Events found', 'events'),
					'not_found_in_trash' => __('No Events found in Trash', 'events')
				),
				'public' => true,
				'hierarchical' => true,
				'has_archive' => true,
				'supports' => array(
					'title',
					'editor',
					'thumbnail'
				),
				'taxonomies' => array( 'category' ),
				'can_export' => true
			));
	}

	add_action('init', 'create_post_type_gallery');

	function create_post_type_gallery(){
		register_taxonomy_for_object_type('category', 'Gallery');
		register_taxonomy_for_object_type('post_tag', 'gallery');
		register_post_type('gallery',
			array(
				'labels' => array(
					'name' => __('Gallery', 'gallery'),
					'singular_name' => __('Gallery', 'gallery'),
					'add_new' => __('Add New Gallery', 'gallery'),
					'add_new_item' => __('Add New Gallery', 'gallery'),
					'edit' => __('Edit', 'gallery'),
					'edit_item' => __('Edit Gallery', 'gallery'),
					'new_item' => __('New Gallery', 'gallery'),
					'view' => __('View Gallery', 'gallery'),
					'view_item' => __('View Gallery', 'gallery'),
					'search_items' => __('Search Gallery', 'gallery'),
					'not_found' => __('No Gallery found', 'gallery'),
					'not_found_in_trash' => __('No Gallery found in Trash', 'gallery')
				),
				'public' => true,
				'hierarchical' => true,
				'has_archive' => false,
				'menu_icon'   => 'dashicons-images-alt2',
				'exclude_from_search' => true,
				'supports' => array(
					'title',
					'editor',
					'thumbnail'
				),
				'taxonomies' => array( 'category' ),
				'can_export' => true
			));
	}


	function nav($location, $menu, $class, $after=null){
		wp_nav_menu(
		array(
			'theme_location'  => $location,
			'menu'            => $menu,
			'container'       => 'div',
			'container_class' => $class,
			'container_id'    => '',
			'menu_class'      => 'menu',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => $after,
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul>%3$s</ul>',
			'depth'           => 0,
			'walker'          => ''
			)
		);
	}

	function site_styles(){
		wp_register_style('sans', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700', array(), '1.0', 'all');
		wp_enqueue_style('sans');
	}

	add_action('wp_enqueue_scripts', 'site_styles');


	function selectedDate($format = 0){
		$year = $_REQUEST['aasta'];
		$month = $_REQUEST['kuu'];
		$day = $_REQUEST['paev'];
		if($format = 1) {
			$return = $day . '.' . $month . '.' . $year;
		} else {
			$return = $day . ' ' . getMonth($month) . ' ' . $year;
		}

		return $return;
	}

	function getMonth($month){
		switch($month):
			case '1':
				$return = 'JAANUAR';
				break;
			case '2':
				$return = 'VEEBRUAR';
				break;
			case '3':
				$return = 'MÄRTS';
				break;
			case '4':
				$return = 'APRILL';
				break;
			case '5':
				$return = 'MAI';
				break;
			case '6':
				$return = 'JUUNI';
				break;
			case '7':
				$return = 'JUULI';
				break;
			case '8':
				$return = 'AUGUST';
				break;
			case '9':
				$return = 'SEPTEMBER';
				break;
			case '10':
				$return = 'OKTOOBER';
				break;
			case '11':
				$return = 'NOVEMBER';
				break;
			case '12':
				$return = 'DETSEMBER';
				break;
		endswitch;
		return $return;
	}

	function getEvents(){
		$args = array(
			'posts_per_page'   => 10000,
			'offset'           => 0,
			'category'         => '',
			'category_name'    => '',
			'orderby'          => 'date',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => '',
			'meta_value'       => '',
			'post_type'        => 'events',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'		   => '',
			'author_name'	   => '',
			'post_status'      => 'publish',
			'suppress_filters' => true
		);
		$posts_array = get_posts( $args );
		return $posts_array;
	}

	function getTopicEvents($value){
		$args = array(
			'posts_per_page'   => 10000,
			'offset'           => 0,
			'category'         => '',
			'category_name'    => '',
			'orderby'          => 'date',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => 'type',
			'meta_value'       => $value,
			'post_type'        => 'events',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'		   => '',
			'author_name'	   => '',
			'post_status'      => 'publish',
			'suppress_filters' => true
		);
		$posts_array = get_posts( $args );
		return $posts_array;
	}

	function getFrontpageEvents(){
		$args = array(
			'posts_per_page'   => 10000,
			'offset'           => 0,
			'category'         => '',
			'category_name'    => 'Avalehe Sündmused',
			'orderby'          => 'date',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => '',
			'meta_value'       => '',
			'post_type'        => 'events',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'		   => '',
			'author_name'	   => '',
			'post_status'      => 'publish',
			'suppress_filters' => true
		);
		$posts_array = get_posts( $args );
		return $posts_array;
	}

	function frontpageView(){
		$events = getFrontpageEvents();
		if($events):
			echo FeaturedEvents('theater', $events, 'teater');
			echo FeaturedEvents('concert', $events, 'kontsert');
			echo FeaturedEvents('cultural events', $events, 'kultuurisundmused');
			echo FeaturedEvents('cinema', $events, 'kino');
			echo FeaturedEvents('miscellaneous', $events, 'varia');
			echo FeaturedEvents('exhibition', $events, 'naitus');
		endif;
	}

	function FeaturedEvents($type, $events, $est){
		$i=0;
		$html = '<div class="frontpage_events_container type-' . $type . '">';
			$html .= '<div class="frontpage_events_header">';
				$html .= '<h3>' . $est . '</h3>';
				$html .= '<a href="' . get_site_url() . '/' . $est . '">Vaata lisa</a>';
			$html .= '</div>';
			$html .= '<div class="frontpage_events_main">';

				$eventsSorted = FeaturedEventsSorter($type, $events);

				foreach($eventsSorted as $event):
					$i++;
					if($i <= 2):
						$html .= '<div class="frontpage_event_single" data-target="' . get_permalink($event->ID) . '">';
							$html .= '<div class="event-side">';
								if(get_the_post_thumbnail($event->ID, 'large', 'string')):
									$html .= get_the_post_thumbnail($event->ID, 'large', 'string');
								else:
									$html .= '<div class="post_thumbnail_image"><img src="' . get_template_directory_uri() . '/images/logo.png"/></div>';
								endif;
								$html .= '<h2>' . $event->post_title . '</h2>';
							$html .= '</div>';
							$html .= '<div class="event-side">';
							if(get_field('dates', $event->ID)):
								if(get_field('ajavahemik', $event->ID)['0'] == 'yes'):
									$dates = get_field('dates', $event->ID);
									$start = $dates['0']['date'];
									$startTime = $dates['0']['times']['0']['time'];
									$end = $dates['1']['date'];
									$endTime = $dates['0']['times']['1']['time'];
									$eventFilter = ifPassed($start, $end);
									if($eventFilter == '1'):
										$html .= '<span class="event-date">' . getEDate($start) . ' - ' . '</br>' . getEDate($end) . '</span>';
										$html .= '<span class="event-time">' . $startTime . ' - ' . $endTime . '</span>';
									endif;
								else:
									$fieldDates = get_field('dates', $event->ID);
									foreach($fieldDates as $fieldDate):
										$eventFilter = ifPassed($fieldDate['date']);
										if($eventFilter == '1'):
											$html .= '<span class="event-date">' . getEDate($fieldDate['date']) . '</span>';
											if($fieldDate['times']):
												$times = $fieldDate['times'];
												foreach($times as $time):
													$html .= '<span class="event-time">' . $time['time'] . '</span>';
												endforeach;
											endif;
										endif;
									endforeach;
								endif;
							endif;
							$html .= '</div>';
						$html .= '</div>';
					endif;
				endforeach;
			$html .= '</div>';
		$html .= '</div>';
		if($i > 0){
			return $html;
		} else {
			return false;
		}
	}

	function FeaturedEventsSorter($type, $events) {
		$eventsArray = array();

		foreach($events as $event):
			$get_date = get_field('dates', $event->ID);

			if($type == get_field('type', $event->ID)):
				if(get_field('dates', $event->ID)):
					while(has_sub_field('dates', $event->ID)):
						$getFirstDate = get_sub_field('date', $event->ID);
						$eventFilter = ifPassed($getFirstDate);

						if($eventFilter == '1'):
							$date = strtotime($getFirstDate);
							$eventsArray[]= array('ID'=>$event->ID, 'date'=>$date);
							break;
						endif;
					endwhile;
				endif;
			endif;
		endforeach;

		usort($eventsArray, function($a, $b) {
			return $a['date'] - $b['date'];
		});

		$eventsSorted = array();
		foreach($eventsArray as $event):
			$content_post = get_post($event['ID']);
			$eventsSorted[]= $content_post;
		endforeach;

		return $eventsSorted;
	}

	function selectedEvents(){
		$events = getEvents();
		$eventsSorted = PostOrderingByDate($events);
		$selectedDate = selectedDate('1');
		if($events):
			foreach($eventsSorted as $event):
				$range = controllRange($event->ID);
				$filter = filterByDate($range, $event->ID);
				if($filter == '1'):
					$html .= '<div class="event-item" data-target="' . get_permalink($event->ID) . '">';
						$html .= '<div class="event-side">';
							if(get_the_post_thumbnail($event->ID, 'large', 'string')):
								$html .= get_the_post_thumbnail($event->ID, 'large', 'string');
							else:
								$html .= '<div class="post_thumbnail_image"><img src="' . get_template_directory_uri() . '/images/logo.png"/></div>';
							endif;
							$html .= '<h2>' . $event->post_title . '</h2>';
							if(get_field('buy_ticket_link', $event->ID)):
								$html .= '<a class="event-buy-ticket" href="' . get_field('buy_ticket_link', $event->ID) . '">Osta pilet</a>';
							endif;
						$html .= '</div>';
						$html .= '<div class="event-side">';
							if(get_field('dates', $event->ID)):
								if(get_field('ajavahemik', $event->ID)['0'] == 'yes'):
									$dates = get_field('dates', $event->ID);
									$start = $dates['0']['date'];
									$startTime = $dates['0']['times']['0']['time'];
									$end = $dates['1']['date'];
									$endTime = $dates['1']['times']['0']['time'];
									$eventFilter = ifPassed($start, $end);
									if($eventFilter == '1'):
										$html .= '<span class="event-date">' . getEDate($start) . ' - ' . '</br>' . getEDate($end) . '</span>';
										$html .= '<span class="event-time">' . $startTime . ' - ' . $endTime . '</span>';
									endif;
								else:
									while(has_sub_field('dates', $event->ID)):
										$eventDate = get_sub_field('date', $event->ID);
										$eventFilter = ifPassed($eventDate);
										$visualEventDate = getEDate(get_sub_field('date', $event->ID));
										if($eventFilter == '1' && $selectedDate == $visualEventDate):
											$html .= '<span class="event-date">' . $visualEventDate . '</span>';
											if(get_sub_field('times', $event->ID)):
												while(has_sub_field('times', $event->ID)):
													$html .= '<span class="event-time">' . get_sub_field('time') . '</span>';
												endwhile;
											endif;
										endif;
									endwhile;
								endif;
							endif;
						$html .= '</div>';
					$html .= '</div>';
				endif;
			endforeach;
			echo $html;
		endif;
	}

	function getTopicValue($topic){
		switch($topic):
			case 'Teater':
				$return = 'theater';
				break;
			case 'Kino':
				$return = 'cinema';
				break;
			case 'Näitus':
				$return = 'exhibition';
				break;
			case 'Kontsert':
				$return = 'concert';
				break;
			case 'Varia':
				$return = 'miscellaneous';
				break;
			case 'Kultuurisündmused':
				$return = 'cultural events';
				break;
		endswitch;
		return $return;
	}

	function selectedTopicEvents($topic){
		$value = getTopicValue($topic);
		$events = getTopicEvents($value);
		$eventsSorted = PostOrderingByDate($events);
		if($events):
			foreach($eventsSorted as $event):
				$range = controllRange($event->ID);
				$filter = filterEByDateTime($range, $event->ID);
				if($filter == '1'):
					$html .= '<div class="event-item" data-target="' . get_permalink($event->ID) . '">';
						$html .= '<div class="event-side">';
							if(get_the_post_thumbnail($event->ID, 'large', 'string')):
								$html .= get_the_post_thumbnail($event->ID, 'large', 'string');
							else:
								$html .= '<div class="post_thumbnail_image"><img src="' . get_template_directory_uri() . '/images/logo.png"/></div>';
							endif;
							$html .= '<h2>' . $event->post_title . '</h2>';
							if(get_field('free', $event->ID)):
								$html .= '<a class="event-buy-ticket">Üritus on tasuta</a>';
							else:
								if(get_field('buy_ticket_link', $event->ID)):
									$html .= '<a class="event-buy-ticket" href="'. get_field('buy_ticket_link') . '">Osta pilet</a>';
								endif;
							endif;
						$html .= '</div>';
						$html .= '<div class="event-side">';
							if(get_field('dates', $event->ID)):
								if(get_field('ajavahemik', $event->ID)['0'] == 'yes'):
									$dates = get_field('dates', $event->ID);
									$start = $dates['0']['date'];
									$startTime = $dates['0']['times']['0']['time'];
									$end = $dates['1']['date'];
									$endTime = $dates['0']['times']['1']['time'];
									$eventFilter = ifPassed($start, $end);
									if($eventFilter == '1'):
										$html .= '<span class="event-date">' . getEDate($start) . ' - ' . '</br>' . getEDate($end) . '</span>';
										$html .= '<span class="event-time">' . $startTime . ' - ' . $endTime . '</span>';
									endif;
								else:
									while(has_sub_field('dates', $event->ID)):
										$eventDate = get_sub_field('date', $event->ID);
										$eventFilter = ifPassed($eventDate);
										if($eventFilter == '1'):
											$html .= '<span class="event-date">' . getEDate(get_sub_field('date', $event->ID)) . '</span>';
											if(get_sub_field('times', $event->ID)):
												while(has_sub_field('times', $event->ID)):
													$html .= '<span class="event-time">' . get_sub_field('time') . '</span>';
												endwhile;
											endif;
										endif;
									endwhile;
								endif;
							endif;
						$html .= '</div>';
					$html .= '</div>';
				endif;
			endforeach;

			echo $html;
		endif;
	}

	function allEvents($topic){
		$value = getTopicValue($topic);
		$events = getTopicEvents($value);
		$eventsSorted = PostOrderingByDate($events);
		$monthNr = 0;
		if($events):
			$forwardMonths = [];
			for ($i = 0; $i < 12; $i++) {
				array_push($forwardMonths, date('m', strtotime("+$i month")));
			}

			foreach($forwardMonths as $forwardMonth):
				foreach($eventsSorted as $event):
					$range = controllRange($event->ID);
					$filter = filterEByDate($range, $event->ID);

					if($filter == '1'):
						$eventDates = array();
						if(get_field('dates', $event->ID)):
							$dates = get_field('dates', $event->ID);

							if(get_field('ajavahemik', $event->ID)['0'] == 'yes'):
								$ajavahemik = get_field('ajavahemik', $event->ID)['0'];

								$date = $dates['0']['date'];
								$endDate = $dates['1']['date'];
								$datediff = $endDate - $date;
								$startTime = $dates['0']['times']['0']['time'];
								$endTime = $dates['0']['times']['1']['time'];
								$startEndTime = array($startTime, $endTime);

								for($i = 1; $i < $datediff; $i++){
									$times = array();
									$date = date('Ymd', strtotime('+1 day', strtotime($date)));

									$times[]= array('time' => $startEndTime);
									$dates[]= array('date'=>$date, 'times'=>$times, 'ajavahemik' => '1');
								}
							endif;
							$counter = 0;
							foreach($dates as $date) :
								$getDate = $date['date'];
								$times = array();
								$dateTimes = array();
								$timesArray = $date['times'];

								if($timesArray):
									foreach($timesArray as $time):
										array_push($times, $time);
									endforeach;
								endif;

								$dateTimes[]= array('date'=>$getDate, 'times'=>$times);

								array_push($eventDates, $dateTimes);
							endforeach;
						endif;

						foreach($eventDates as $eventDate):

							$date = $eventDate[0]['date'];
							$times = $eventDate[0]['times'];

							$eventDateTime = date('d.m.Y', strtotime($date));
							$eventDateMonth = date('m', strtotime($date));

							if($eventDateMonth == $forwardMonth):
								$eventsHtml[$eventDateTime] .= '<div class="all-events-item">';
									$eventsHtml[$eventDateTime] .= '<div class="event-side">';
										$date = get_sub_field('date', $event->ID);
										$eventsHtml[$eventDateTime] .= '<span class="event-date">' . $eventDateTime . '</span>';
										$eventsHtml[$eventDateTime] .= '<div class="event-times">';
											if(get_field('ajavahemik', $event->ID)['0'] == 'yes'):
												$eventsHtml[$eventDateTime] .= '<span class="event-time">' . $time['time']['0'] . ' - ' . $time['time']['1'] . '</span>';
											elseif($times):
												foreach($times as $time):
													$eventsHtml[$eventDateTime] .= '<span class="event-time">' . $time['time'] . '</span>';
												endforeach;
											endif;
											$eventsHtml[$eventDateTime] .= '</div>';
									$eventsHtml[$eventDateTime] .= '</div>';
									$eventsHtml[$eventDateTime] .= '<div class="event-side">';
										$eventsHtml[$eventDateTime] .= '<h2>' . $event->post_title . ' ' . '<a href="' . get_permalink($event->ID) . '">VAATA</a>' . '</h2>';
									$eventsHtml[$eventDateTime] .= '</div>';
								$eventsHtml[$eventDateTime] .= '</div>';
							endif;
						endforeach;
					endif;
				endforeach;

				if($eventsHtml):
					$monthsEst = monthsEst($forwardMonth);
					$html .= '<p class="month">' . $monthsEst . '</p>';
				endif;

				asort($eventsHtml);
				$html .= join($eventsHtml);
				$eventsHtml = array();
			endforeach;

			echo $html;
		endif;
	}

	function getEventsCategoryPosts($category){
		$args = array(
			'posts_per_page'   => 10000,
			'offset'           => 0,
			'category'         => '',
			'category_name'    => $category,
			'orderby'          => 'date',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => 'type',
			'post_type'        => 'events',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'		   => '',
			'author_name'	   => '',
			'post_status'      => 'publish',
			'suppress_filters' => true
		);
		$posts_array = get_posts( $args );
		return $posts_array;
	}

	function PostOrderingByDate($events){
		$eventsArray = array();

		foreach($events as $event):
			$get_date = get_field('dates', $event->ID);

			if(get_field('dates', $event->ID)):
				while(has_sub_field('dates', $event->ID)):
					$getDate = get_sub_field('date', $event->ID);
					$eventFilter = ifPassed($getDate);

					if(get_sub_field('times', $event->ID)):
						while(has_sub_field('times', $event->ID)):
							$getTime = get_sub_field('time', $event->ID);
							$time = strtotime($getTime);

							$dateAndtime = $getDate . $getTime;
							$timeFilter = ifPassedDateTime($dateAndtime);
						endwhile;
					endif;

					if($eventFilter == '1'):
						break;
					endif;
				endwhile;
			endif;

			if($getTime):
				$datetime = strtotime($getDate . ' ' . $getTime);
				$readableDateTime = date("H:i d.m.Y", $datetime);

				$eventsArray[]= array('ID'=>$event->ID, 'date'=>$datetime);
			else:
				$date = strtotime($getDate);
				$eventsArray[]= array('ID'=>$event->ID, 'date'=>$date);
			endif;
		endforeach;

		usort($eventsArray, function($a, $b) {
			return $a['date'] - $b['date'];
		});

		$eventsSorted = array();
		foreach($eventsArray as $event):
			$content_post = get_post($event['ID']);
			$eventsSorted[]= $content_post;
		endforeach;

		return $eventsSorted;
	}

	function months($date) {
		$month = date("m", strtotime($date));
		return $month;
	}

	function monthsEst($month) {
		if($month == "01") {
			return "Jaanuar";
		} elseif($month == "02") {
			return "Veebruar";
		} elseif($month == "03") {
			return "Märts";
		} elseif($month == "04") {
			return "Aprill";
		} elseif($month == "05") {
			return "Mai";
		} elseif($month == "06") {
			return "Juuni";
		} elseif($month == "07") {
			return "Juuli";
		} elseif($month == "08") {
			return "August";
		} elseif($month == "09") {
			return "September";
		} elseif($month == "10") {
			return "Oktoober";
		} elseif($month == "11") {
			return "November";
		} elseif($month == "12") {
			return "Detsember";
		}
	}

	function filterEByDate($range, $id){
		if(get_field('dates', $id)):
			while(has_sub_field('dates', $id)):
				$date = get_sub_field('date', $id);
			endwhile;
		endif;

		$return = ifPassed($date);

		return $return;
	}

	function filterEByDateTime($range, $id){
		if(get_field('dates', $id)):
			while(has_sub_field('dates', $id)):
				$date = get_sub_field('date', $id);
				if(get_sub_field('times', $event->ID)):
					while(has_sub_field('times', $event->ID)):
						$getTime = get_sub_field('time', $event->ID);
						$time = strtotime($getTime);

						$dateAndtime = $date . ' ' . $getTime;
					endwhile;
				endif;
			endwhile;
		endif;

		$return = ifPassedDateTime($dateAndtime);

		return $return;
	}

	function getEDate($date){
		$dt = new DateTime($date);
		return $dt->format('d.m.Y');
	}

	function filterByDate($range, $id){
		$date = [];

		if(get_field('dates', $id)):
			while(has_sub_field('dates', $id)):
				array_push($date, get_sub_field('date', $id));
			endwhile;
		endif;

		if(get_field('ajavahemik', $id)['0'] == 'yes'):
			$return = ifEqual($date['0'], $date['1']);
		else:
			$return = ifEqual($date);
		endif;

		return $return;
	}

	function ifPassed($start=null, $end=null){
		$current = date('Ymd');
		if($end){
			$return = ($current <= $end) ? true : false;
		} else {
			$return = ($current <= $start) ? true : false;
		}
		return $return;
	}

	function ifPassedDateTime($start){
		date_default_timezone_set('Europe/Tallinn');
		$current = date('Ymd H:i');
		$return = ($current <= $start) ? true : false;
		return $return;
	}

	function ifEqual($start=null, $end=null){
		$selected = $_REQUEST['aasta'] . $_REQUEST['kuu'] . $_REQUEST['paev'];
		if($end){
			$return = (($start <= $selected) && ($selected <= $end)) ? true : false;
		} elseif(is_array($start)) {
			foreach($start as $eventDate) {
				if($selected == $eventDate) {
					$return = true;
				}
			}
		} else {
			$return = ($selected == $start) ? true : false;
		}
		return $return;
	}

	function controllRange($id){
		if(get_field('dates', $id)):
            while(has_sub_field('dates', $id)):
                $return = (get_sub_field('date', $id)) ? '1' : '0';
            endwhile;
		endif;

		return $return;
	}

?>