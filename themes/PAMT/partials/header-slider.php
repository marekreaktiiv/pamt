<div class="slider-wrap">
    <div class="slider" id="slider">
        <div class="holder">
            <?php $slider_gallery = get_field('slider_gallery'); ?>
            <?php $counter = 0; ?>

            <?php foreach ($slider_gallery as $image) : ?>
                <div class="slide" id="slide-<?php echo $counter; ?>" style="background-image: url('<?php echo $image['url']; ?>');">
                    <?php $counter++; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <nav class="slider-nav">
        <?php $counter = 0; ?>

        <?php foreach ($slider_gallery as $image) : ?>
            <a href="#slide-<?php echo $counter; ?>">Slide</a>
            <?php $counter++; ?>
        <?php endforeach; ?>
  </nav>
<script>

jQuery(document).ready(function() {
    timer();

    jQuery('.slider-nav a').click(function(){
        clearInterval(interval);
        timer();
    });
});

var windowWidth = jQuery(window).width();
var counter = <?php echo $counter; ?>;

jQuery('.slide').width(windowWidth);
jQuery('.holder').width(windowWidth * counter);
jQuery('.slider-nav').find('a').first().addClass('active');

var slider = {
    el: {
        slider: $("#slider"),
        allSlides: $(".slide"),
        sliderNav: $(".slider-nav"),
        allNavButtons: $(".slider-nav > a")
    },

    timing: 800,
    slideWidth: windowWidth,

    init: function() {
        this.bindUIEvents();
    },

    bindUIEvents: function() {
        this.el.sliderNav.on("click", "a", function(event) {
        slider.handleNavClick(event, this);
        });
    },

    handleNavClick: function(event, el) {
        event.preventDefault();
        var position = $(el).attr("href").split("-").pop();

        this.el.slider.animate({
        scrollLeft: position * this.slideWidth
        }, this.timing);

        this.changeActiveNav(el);
    },

    changeActiveNav: function(el) {
        this.el.allNavButtons.removeClass("active");
        $(el).addClass("active");
    }

};

slider.init();

var interval;
var timer = function(){
    interval = setInterval(function(){
        if(jQuery('.slider-nav').find('.active').next().is('a'))  {
            jQuery('.slider-nav').find('.active').next().trigger('click');
        } else {
            jQuery('.slider-nav').find('a').first().trigger('click');
        }
    }, 5000);
};
</script>

</div>