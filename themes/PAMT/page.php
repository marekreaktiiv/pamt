<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<section>
			<div class="post" id="post-<?php the_ID(); ?>">
				<div class="wrapper">
					<div class="column cs-75">
						<div class="entry"><?php the_content(); ?></div>
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
		</section>
	<?php endwhile; endif; ?>

<?php get_footer(); ?>

